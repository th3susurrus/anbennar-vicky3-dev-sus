﻿cannorian_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { cannorian_heritage dwarven_race_heritage elven_race_heritage gnomish_race_heritage halfling_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

cannorian_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { cannorian_heritage aelantiri_heritage dwarven_race_heritage elven_race_heritage gnomish_race_heritage halfling_race_heritage goblin_race_heritage gnollish_race_heritage kobold_race_heritage orcish_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

bulwari_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { bulwari_heritage dwarven_race_heritage elven_race_heritage harpy_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

bulwari_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { bulwari_heritage dwarven_race_heritage elven_race_heritage harpy_race_heritage aelantiri_heritage goblin_race_heritage gnollish_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

sarhali_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { west_sarhaly_heritage east_sarhaly_heritage south_sarhaly_heritage harpy_race_heritage halfling_race_heritage gnomish_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

sarhali_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { west_sarhaly_heritage east_sarhaly_heritage south_sarhaly_heritage harpy_race_heritage halfling_race_heritage gnomish_race_heritage gnollish_race_heritage lizardman_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

rahen_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { raheni_heritage east_sarhaly_heritage southeast_halessi_heritage dwarven_race_heritage hobgoblin_race_heritage harimari_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

rahen_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { raheni_heritage east_sarhaly_heritage southeast_halessi_heritage dwarven_race_heritage hobgoblin_race_heritage harimari_race_heritage northeast_halessi_heritage bulwari_heritage goblin_race_heritage orcish_race_heritage kobold_race_heritage lizardman_race_heritage gnomish_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

fp_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { fp_heritage northeast_halessi_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

fp_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { fp_heritage northeast_halessi_heritage goblin_race_heritage centaur_race_heritage harpy_race_heritage ogre_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

gerudian_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { cannorian_heritage dwarven_race_heritage elven_race_heritage aelantiri_heritage harpy_race_heritage troll_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

gerudian_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { cannorian_heritage dwarven_race_heritage elven_race_heritage aelantiri_heritage harpy_race_heritage troll_race_heritage orcish_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

haless_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { northeast_halessi_heritage southeast_halessi_heritage dwarven_race_heritage raheni_heritage fp_heritage harpy_race_heritage hobgoblin_race_heritage kobold_race_heritage harimari_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

haless_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { northeast_halessi_heritage southeast_halessi_heritage dwarven_race_heritage raheni_heritage fp_heritage harpy_race_heritage hobgoblin_race_heritage kobold_race_heritage harimari_race_heritage elven_race_heritage orcish_race_heritage goblin_race_heritage ogre_race_heritage troll_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

aelantiri_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { aelantiri_heritage elven_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

aelantiri_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { aelantiri_heritage elven_race_heritage northeast_halessi_heritage southeast_halessi_heritage dwarven_race_heritage raheni_heritage fp_heritage harpy_race_heritage kobold_race_heritage cannorian_heritage west_sarhaly_heritage east_sarhaly_heritage south_sarhaly_heritage bulwari_heritage gnomish_race_heritage goblin_race_heritage orcish_race_heritage halfling_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}


all_heritage_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { aelantiri_heritage elven_race_heritage northeast_halessi_heritage southeast_halessi_heritage dwarven_race_heritage raheni_heritage fp_heritage harpy_race_heritage kobold_race_heritage cannorian_heritage west_sarhaly_heritage east_sarhaly_heritage south_sarhaly_heritage bulwari_heritage gnomish_race_heritage goblin_race_heritage orcish_race_heritage halfling_race_heritage gnollish_race_heritage centaur_race_heritage hobgoblin_race_heritage ogre_race_heritage troll_race_heritage fungoid_race_heritage lizardman_race_heritage minotaur_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_dwarf_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { dwarven_race_heritage cannorian_heritage bulwari_heritage raheni_heritage harimari_race_heritage aelantiri_heritage elven_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_dwarf_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { dwarven_race_heritage cannorian_heritage bulwari_heritage raheni_heritage harimari_race_heritage aelantiri_heritage elven_race_heritage goblin_race_heritage orcish_race_heritage harpy_race_heritage hobgoblin_race_heritage troll_race_heritage ogre_race_heritage kobold_race_heritage halfling_race_heritage gnomish_race_heritage fungoid_race_heritage minotaur_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_goblin_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { goblin_race_heritage raheni_heritage northeast_halessi_heritage dwarven_race_heritage orcish_race_heritage harpy_race_heritage hobgoblin_race_heritage troll_race_heritage kobold_race_heritage harimari_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_goblin_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { goblin_race_heritage raheni_heritage northeast_halessi_heritage dwarven_race_heritage orcish_race_heritage harpy_race_heritage hobgoblin_race_heritage troll_race_heritage kobold_race_heritage harimari_race_heritage cannorian_heritage aelantiri_heritage elven_race_heritage gnollish_race_heritage ogre_race_heritage fungoid_race_heritage minotaur_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_orc_tolerance = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { orcish_race_heritage goblin_race_heritage gnollish_race_heritage harpy_race_heritage kobold_race_heritage minotaur_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}

dwarovar_orc_tolerance_plus = {	
	color={ 0 0 0 }
	religion = fey_court
	traits = { orcish_race_heritage goblin_race_heritage gnollish_race_heritage harpy_race_heritage kobold_race_heritage minotaur_race_heritage cannorian_heritage bulwari_heritage raheni_heritage dwarven_race_heritage elven_race_heritage aelantiri_heritage hobgoblin_race_heritage ogre_race_heritage troll_race_heritage harimari_race_heritage fungoid_race_heritage }
	male_common_first_names = {
		Bob
	}
	female_common_first_names = {
		Bobbette
	}
	noble_last_names = {
		
	}
	common_last_names = { #
		Builder
	}
	graphics = european
	ethnicities = {
		1 = caucasian
	}
}