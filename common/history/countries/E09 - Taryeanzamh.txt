﻿COUNTRIES = {
	c:E09 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = centralization

		effect_starting_politics_traditional = yes
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_same_heritage_only
		activate_law = law_type:law_traditional_magic_banned
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_migration_controls
	}
}