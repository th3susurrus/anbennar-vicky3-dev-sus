﻿COUNTRIES = {
	c:L65 ?= {
		effect_starting_technology_tier_5_tech = yes
		
		effect_starting_politics_traditional = yes

		# Laws
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slave_trade

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_artifice_banned
	}
}