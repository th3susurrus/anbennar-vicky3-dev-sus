﻿COUNTRIES = {
	c:D33 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = colonization
		add_technology_researched = academia
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		activate_law = law_type:law_local_police
		# No schools
		# No health system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_artifice

		set_institution_investment_level = {
			institution = institution_police
			level = 2
		}
	}
}