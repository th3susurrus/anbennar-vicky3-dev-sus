﻿COUNTRIES = {
	c:A29 ?= {
		effect_starting_technology_tier_3_tech = yes	#doc says lagging behind rest of Escann in terms of institutions and economy
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_professional_army

		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_nation_of_magic
	}
}