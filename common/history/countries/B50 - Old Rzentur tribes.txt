﻿COUNTRIES = {
	c:B50 ?= {
		set_state_religion = rel:vechnogejzn
		effect_starting_technology_tier_6_tech = yes
        effect_starting_politics_traditional = yes
        effect_native_conscription_4 = yes
	}
}