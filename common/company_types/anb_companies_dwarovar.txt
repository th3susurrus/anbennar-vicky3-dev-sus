﻿company_strongbellow_shipyard = {
	icon = "gfx/interface/icons/company_icons/basic_shipyards.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_harbor_shipbuilding.dds"

	flavored_company = yes

	building_types = {
		building_shipyards
		building_military_shipyards
		building_motor_industry
	}

	potential = {
		has_interest_marker_in_region = region_bahar
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_OVDAL_TUNGR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_OVDAL_TUNGR
			any_scope_building = {
				is_building_type = building_motor_industry
				level >=4
			}
		}
	}

	prosperity_modifier = {
		country_convoys_capacity_mult = 0.1
		building_coal_mine_throughput_add = 0.05
	}

	ai_weight = {
		value = 3
	}
}

company_massive_shaft = { #Massive Shaft Deep Drilled Fracking
	icon = "gfx/interface/icons/company_icons/basic_mineral_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_doodad_manufacturies
	}

	potential = {
		has_interest_marker_in_region = region_mountainheart
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_DEAD_CAVERNS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_DEAD_CAVERNS
			any_scope_building = {
				is_building_type = building_doodad_manufacturies
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		state_pollution_reduction_health_mult = 0.1
		building_group_bg_mining_throughput_add = 0.1
	}

	ai_weight = {
		value = 3
	}
}

company_basic_fungiculture = {
	icon = "gfx/interface/icons/company_icons/basic_agriculture_1.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_rice.dds"
	
	uses_dynamic_naming = yes
	
	dynamic_company_type_names = {
		"dynamic_company_type_company"
		"dynamic_company_type_consortium"
		"dynamic_company_type_guild"
		"dynamic_company_type_society"
		"dynamic_company_type_fellowship"
	}
	
	building_types = {  
		building_mushroom_farm
		building_serpentbloom_farm	
	}

	possible = { 
		any_scope_state = {
			any_scope_building = {
				OR = {
					is_building_type = building_mushroom_farm
					is_building_type = building_serpentbloom_farm
				}
				level >= 3
			}
		}
	}
	
	prosperity_modifier = {
		state_harvest_condition_veladklufar_swarm_mult = -0.25
	}	
}