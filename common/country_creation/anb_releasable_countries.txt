﻿A03 = { # Lorent
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A05 = { # Bisan
	states = {
		STATE_BISAN
	}
	
	ai_will_do = { always = no }
}

A06 = { # Gnomish Hierarchy
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A07 = { #Reveria
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A08 = { #Eborthil
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A09 = { #Busilar
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
} 

A10 = { #Grombar
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
} 

A14 = { #Small Country
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A15 = { #Portnamm
	states = {
		STATE_IOCHAND
	}
	
	ai_will_do = { always = no }
}

A16 = { #Kobildzan
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A18 = { #Bayvek
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A19 = { #Vertesk
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A20 = { # Ibevar
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A21 = { # Farranean
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A22 = { # Ancardia
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A23 = { # Elikhand
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A24 = { # Newshire
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A25 = { # Araionn
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A26 = { # Wyvernheart
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A27 = { # Blademarches
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A28 = { # Rosande
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A29 = { # Marrhold
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A30 = { # Magocratic Demesne / Nurcestir?
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A31 = { # Unguldavor
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A32 = { # Silvermere
	states = {
		STATE_BURNOLL
	}
	
	ai_will_do = { always = no }
}

A33 = { # Ravelian Rectorate
	states = {
		STATE_PASHAINE #???
	}
	
	ai_will_do = { always = no }
}

A34 = { # Damescrown
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A35 = { # Uelaire
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A36 = { # The Reach
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A37 = { # Silverforge
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A38 = { # Stalbór
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A39 = { # Vrorenmarch
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A40 = { # Luciande
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A41 = { # Wex
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A42 = { # Iochand
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A43 = { # Beepeck
	states = {
		STATE_BEEPECK
	}
	
	ai_will_do = { always = no }
}

A44 = { # Moonhaven
	states = {
		STATE_MOONHAVEN
	}
	
	ai_will_do = { always = no }
}

A45 = { # Pearlsedge
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A46 = { # Giberd
	states = {
		STATE_BENNON
	}
	
	ai_will_do = { always = no }
}

A47 = { # Barumand
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A48 = { # Verne
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A49 = { # Carneter
	states = {
		STATE_NECKCLIFFE
		STATE_WESDAM
		STATE_ROILSARD
		STATE_PEARLSEDGE
		STATE_CARNETER
	}
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A50 = { # Wesdam
	use_culture_states = yes

	ai_will_do = { always = no }
}

A51 = { # Istralore
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A52 = { # Vaengheim
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A55 = { # Pashaine
	states = {
		STATE_PASHAINE
	}
	
	ai_will_do = { always = no }
}

A57 = { # Dameria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A58 = { # Gawed
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A59 = { # Westmoors
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A62 = { # Bjarnrik
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A63 = { # Obrtrol
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A64 = { # Olavlund
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A65 = { # Redglades
	states = {
		STATE_REDGLADES
	}
	ai_will_do = { always = no }
}

A66 = { # Corvuria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A67 = { #Arannen
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A68 = { # Arbaran
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A69 = { # Esmaria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A11 = { # Honderaak
	use_culture_states = yes

	ai_will_do = { always = no }
}

A72 = { # Celmaldor
	states = {
		STATE_SERPENTSHEAD
	}

	ai_will_do = { always = no }
}

A73 = { # Sorncost
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A74 = { # Roilsard
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A78 = { # Deranne
	use_culture_states = yes

	required_num_states = 1 #Deranne proper is always enough (as they have a lot of small island homelands)
	
	ai_will_do = { always = no }
}

B19 = { # Cestirmark
	states = {
		STATE_CESTIRMARK
		STATE_TROLLSBRIDGE
	}

	ai_will_do = { always = no }
}

B20 = { # Marlliande
	states = {
		STATE_NORTH_MARLLIANDE
		STATE_SOUTH_MARLLIANDE
	}

	ai_will_do = { always = no }
}

B22 = { # Zanlib
	use_culture_states = yes

	ai_will_do = { always = no }
}

B23 = { # Isobelin
	states = {
		STATE_ISOBELIN
	}

	ai_will_do = { always = no }
}

B24 = { # Thilvis
	use_culture_states = yes

	ai_will_do = { always = no }
}

B25 = { # Valorpoint
	states = {
		STATE_VALORPOINT
	}

	ai_will_do = { always = no }
}

B48 = { # Ebenmas
	states = {
		STATE_EBENMAS
		STATE_EKRSOKA
	}

	ai_will_do = { always = no }
}

B44 = { # Tellumtir
	states = {
		STATE_TELLUMTIR
		STATE_UZOO
	}

	ai_will_do = { always = no }
}

B92 = { #Sornicande
	states = {
		STATE_SORNICANDE
		STATE_REANNIA
	}

	#required_num_states = 2
		
	ai_will_do = { always = no }
}

B93 = { #Kwinelliande
	states = {
		STATE_KWINELLIANDE
	}
		
	ai_will_do = { always = no }
}

B94 = { #Spoorland
	states = {
		STATE_SPOORLAND STATE_BOEK
	}
		
	ai_will_do = { always = no }
}

B97 = { #Dolindha
	states = {
		STATE_ROGAIDHA STATE_VITREYNN STATE_USLAD STATE_MOCEPED STATE_ARGANJUZORN STATE_BRELAR
	}

	required_num_states = 4
		
	ai_will_do = { always = no }
}

G09 = {	#Teira
	states = {
		STATE_SJAVARRUST STATE_DALAIRRSILD STATE_HJORDAL STATE_FLOTTNORD
	}
		
	ai_will_do = { always = no }
}

B10 = {	#Haraf

	states = {
		STATE_NANI_NOLIHE STATE_AUVUL_TADIH STATE_NIDI_BIKEHA STATE_WORMWAY STATE_KITSIL_KINN
	}
	
	required_num_states = 2
		
	ai_will_do = { always = no }
}

B28 = {	#Dokanto

	states = {
		STATE_DISHENTI
	}
		
	ai_will_do = { always = no }
}

B29 = {	#Sarda Empire

	states = {
		STATE_NIZVELS STATE_HRADAPOLERE STATE_YRISRAD STATE_NIZELYNN STATE_EPADARKAN STATE_VYCHVELS STATE_YNNPADH
	}
	
	required_num_states = 2
		
	ai_will_do = { always = no }
}

B31 = {	#Veykoda

	states = {
		STATE_VIZANIRZAG STATE_CORINSFIELD STATE_WEST_TIPNEY STATE_OSINDAIN STATE_NEW_HAVORAL STATE_ARGEZVALE STATE_LETHIR
	}
	
	required_num_states = 1
		
	ai_will_do = { always = no }
}

B37 = {	#Elathael

	states = {
		STATE_ELATHAEL
	}
		
	ai_will_do = { always = no }
}

B40 = {	#Epednar tribes

	states = {
		STATE_ARANTAS STATE_CHIPPENGARD STATE_BEGGASLAND STATE_PLUMSTEAD STATE_TUSNATA STATE_BORUCKY STATE_UZOO STATE_EBENMAS STATE_TELLUMTIR STATE_EKRSOKA
	}
		
	ai_will_do = { always = no }
}

B43 = {	#Psajuweuh - chiunife

	states = {
		STATE_POSKAWA STATE_TASPAS STATE_YECKUNIA STATE_CASNAROG STATE_ESIMOINE STATE_MORGANIA
	}
	
	required_num_states = 1
		
	ai_will_do = { always = no }
}

B49 = {	#Dragon Dominion

	states = {
		STATE_VIZKALADR STATE_POMVASONN STATE_GOMOSENGHA STATE_ARSERGOZHAR STATE_JUZONDEZAN STATE_VERZEL STATE_FARNOR 
	}
		
	ai_will_do = { always = no }
}

B51 = {	#Arrzreth - cursed_one

	states = {
		STATE_VARBUKLAND STATE_KEWDHEMR STATE_HEHADEDPAR STATE_SILDARBAD 
	}
		
	ai_will_do = { always = no }
}

B70 = {	#Quitl - Mayte

	states = {
		STATE_TLACHIBAR STATE_TAMETER STATE_MINAR_YOLLI STATE_OZTENCOST STATE_MANGROVY_COAST
	}
		
	ai_will_do = { always = no }
}

B81 = {	#Eighard

	states = {
		STATE_FOGHARBAC
	}
		
	ai_will_do = { always = no }
}

B82 = {	#Einnsag

	states = {
		STATE_EINNSAG STATE_RAITHLOS
	}
		
	ai_will_do = { always = no }
}

B83 = {	#Sglard

	states = {
		STATE_SGLOLAD
	}
		
	ai_will_do = { always = no }
}

B84 = {	#Arakeprun

	states = {
		STATE_DARHAN STATE_GATHGOB STATE_PELODARD STATE_TRASAND
	}
		
	ai_will_do = { always = no }
}

B85 = {	#Murdkather

	states = {
		STATE_MURDKATHER STATE_DEARKTIR STATE_PASKALA STATE_TRIMTHIR
	}
		
	ai_will_do = { always = no }
}

B96 = {	#Ghrannblath

	states = {
		STATE_TRIMGARB
	}
		
	ai_will_do = { always = no }
}

B90 = {	#Boek

	states = {
		STATE_BOEK STATE_NORTH_MARLLIANDE STATE_UPPER_SELLA STATE_YNNSMOUTH STATE_ZANLIB STATE_ISOBELIN
	}
		
	ai_will_do = { always = no }
}

G10 = {	#Gemradcurt

	states = {
		STATE_GEMRADCURT STATE_KARNEL STATE_DARTIR STATE_ARMONADH STATE_ANHOLTIR
	}
		
	ai_will_do = { always = no }
}

G11 = {	#Jhorgashirr

	states = {
		STATE_JHORGASHIRR
	}
		
	ai_will_do = { always = no }
}

G12 = {	#Slegcal

	states = {
		STATE_GALBHAN
	}
		
	ai_will_do = { always = no }
}

B88 = {	#Fograc

	states = {
		STATE_EGASACH STATE_FOGRACLEAK STATE_TRIMTHIR
	}
		
	ai_will_do = { always = no }
}

#Dwarovar
D02 = { #Krakdhumvror
	states = {
		STATE_KRAKDHUMVROR
	}
	
	ai_will_do = { always = no }
}
D03 = { #Dur Vazhatun
	states = {
		STATE_DUR_VAZHATUN
	}
	
	ai_will_do = { always = no }
}
D04 = { #Orlazam az Dihr
	states = {
		STATE_ORLAZAM_AZ_DIHR
	}
	
	ai_will_do = { always = no }
}
D05 = { #Amldihr
	states = {
		STATE_AMLDIHR
	}
	
	ai_will_do = { always = no }
}
D07 = { #Haraz Orldhum
	states = {
		STATE_HARAZ_ORLDHUM
	}
	
	ai_will_do = { always = no }
}
D08 = { #Khugdihr
	states = {
		STATE_KHUGDIHR
	}
	
	ai_will_do = { always = no }
}
D09 = { #Mithradhum
	states = {
		STATE_MITHRADHUM
	}
	
	ai_will_do = { always = no }
}
D11 = { #Er Natvir
	states = {
		STATE_ER_NATVIR
	}
	
	ai_will_do = { always = no }
}
D12 = { #Hul Jorkad
	states = {
		STATE_HUL_JORKAD
	}
	
	ai_will_do = { always = no }
}
D13 = { #Gor Burad
	states = {
		STATE_GOR_BURAD
	}
	
	ai_will_do = { always = no }
}
D14 = { #Ovdal Lodhum
	states = {
		STATE_OVDAL_LODHUM
	}
	
	ai_will_do = { always = no }
}
D17 = { #Verkal Skomdihr
	states = {
		STATE_VERKAL_SKOMDIHR
	}
	
	ai_will_do = { always = no }
}
D18 = { #Arg Ordstun
	states = {
		STATE_ARG_ORDSTUN
	}
	
	ai_will_do = { always = no }
}
D19 = { #Shazstundihr
	states = {
		STATE_SHAZSTUNDIHR
	}
	
	ai_will_do = { always = no }
}
D20 = { #Orlghelovar
	states = {
		STATE_ORLGHELOVAR
	}
	
	ai_will_do = { always = no }
}
D22 = { #Gor Vazumbrog
	states = {
		STATE_GOR_VAZUMBROG
	}
	
	ai_will_do = { always = no }
}
D23 = { #Hehodovar
	states = {
		STATE_HEHODOVAR
	}
	
	ai_will_do = { always = no }
}
D24 = { #Seghdihr
	states = {
		STATE_SEGHDIHR
	}
	
	ai_will_do = { always = no }
}
D25 = { #Verkal Gulan
	states = {
		STATE_VERKAL_GULAN
	}
	
	ai_will_do = { always = no }
}
D27 = { #Gor Ozumbrog
	states = {
		STATE_GOR_OZUMBROG
	}
	
	ai_will_do = { always = no }
}
D28 = { #Ovdal Kanzad
	states = {
		STATE_OVDAL_KANZAD
	}
	
	ai_will_do = { always = no }
}
D29 = { #Grozumdihr
	states = {
		STATE_GROZUMDIHR
	}
	
	ai_will_do = { always = no }
}
D30 = { #Ovdal az An
	states = {
		STATE_OVDAL_AZ_AN
	}
	
	ai_will_do = { always = no }
}
D32 = { #Hul az Krakazol
	states = {
		STATE_HUL_AZ_KRAKAZOL
	}
	
	ai_will_do = { always = no }
}
D34 = { #Tuwad Dhumankon
	states = {
		STATE_TUWAD_DHUMANKON
	}
	
	ai_will_do = { always = no }
}
D35 = { #Gronstunad
	states = {
		STATE_GRONSTUNAD
	}
	
	ai_will_do = { always = no }
}
D36 = { #Verkal Dromak
	states = {
		STATE_VERKAL_DROMAK
	}
	
	ai_will_do = { always = no }
}
D60 = { #Verkal Kozenad
	states = {
		STATE_VERKAL_KOZENAD
	}
	
	ai_will_do = { always = no }
}
F16 = { #Crathánor
	use_culture_states = yes
	
	ai_will_do = { always = no }
}