﻿je_eordand_diplomatic_unification = {
	icon = "gfx/interface/icons/event_icons/waving_flag.dds"
	should_be_pinned_by_default = yes
	
	group = je_group_foreign_affairs

	scripted_button = je_eordand_diplomacy_unification_button
	scripted_button = je_eordand_contact_fograc_button
	
	is_shown_when_inactive = {
		can_form_nation = B87
	}

	possible = {
		is_subject = no
	}

	immediate = {
		trigger_event = { id = eordan_unification.2 days = 1 }
	}


	complete = {
		custom_tooltip = {
			text = is_eordand_tt
			c:B87 ?= THIS
		}
	}

	on_complete = {
		trigger_event = { id = eordan_unification.3 days = 30 popup = yes }
	}
	
	invalidate = {
		any_scope_diplomatic_play = {
			initiator_is = root
			OR = {
				target_is = c:B81
				target_is = c:B82
				target_is = c:B83
				target_is = c:B84
				target_is = c:B85
				target_is = c:B96
				target_is = c:G10
				target_is = c:G11
				target_is = c:G12
			}
		}
	}
}

je_eordand_green_artificery_je = {
	icon = "gfx/interface/icons/event_icons/event_industry.dds"
	should_be_pinned_by_default = yes
	
	group = je_group_technology
	
	is_shown_when_inactive = {
		any_primary_culture = {
			has_discrimination_trait = eordan
		}
	}

	possible = {
		has_technology_researched = punch_card_artificery
		NOR = {
			has_law = law_type:law_total_separation
			has_law = law_type:law_state_atheism
		}
	}

	immediate = {
		trigger_event = { id = anb_eordand_events.2 days = 1 }
	}


	complete = {
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_arcane_nexus
				level >= 5
			}
			any_scope_building = {
				is_building_type = building_doodad_manufacturies
				level >= 5
			}
		}
		NOR = {
			has_law = law_type:law_total_separation
			has_law = law_type:law_state_atheism
		}
	}

	on_complete = {
		custom_tooltip = {
			text = green_artifice_unlock_tt
			set_variable = green_artifice_unlocked
		}
	}
	
}