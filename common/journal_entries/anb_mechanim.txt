﻿je_mechanim_1 = {
	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	group = je_group_internal_affairs

	is_shown_when_inactive = {
		OR = {
			has_technology_researched = advanced_mechanim
			has_technology_researched = human_rights
		}
	}

	scripted_progress_bar = mechanim_freedom_progress_bar
	scripted_progress_bar = mechanim_revolt_progress_bar

	possible = {
		any_interest_group = {
			is_marginal = no
			OR = {
				law_stance = {
					law = law_type:law_mechanim_freedom
					value > neutral
				}
				any_scope_character = { #update this with any personal ideologies that want to liberate mechanim
						OR = {
							has_role = politician
							has_role = agitator
					}
					OR = {
						has_ideology = ideology:ideology_mechanim_liberator
					}
				}
			}
		}
		has_technology_researched = advanced_mechanim
		has_technology_researched = human_rights
		NOT = {has_variable = mechanim_je_completed}
	}

	immediate = {
		root = {
			save_scope_as = root_country
		}
		set_mechanim_vars = yes
		trigger_event = { id = mechanim_revolt.4 days = 1 popup = yes }
	}

	on_weekly_pulse = {
		effect = {
			calculate_mechanim_revolt_progress = yes
		}
	}

	on_monthly_pulse = {
		random_events = {
			200 = 0
		}
		effect = {
			calculate_mechanim_revolt_progress = yes
		}
	}

	fail = {
		custom_tooltip = {
			text = mechanim_revolt_progress_bar_at_full_tt
			scope:journal_entry = {
				"scripted_bar_progress(mechanim_revolt_progress_bar)" >= 100
			}
		}
	}

	complete = {
		OR = {
			has_law = law_type:law_mechanim_freedom
			custom_tooltip = {
				text = mechanim_sucess_progress_bar_at_full_tt
				scope:journal_entry = {
					"scripted_bar_progress(mechanim_freedom_progress_bar)" >= 100
				}
			}
		}
	}

	on_fail = {
		set_variable = {
			name = mechanim_wrongs_enforced
			value = flag
		}
		set_variable = {
			name = mechanim_oppressor_revolt
			value = flag
		}
		set_variable = {
			name = mechanim_je_completed
			value = flag
		}
		if = {
			limit = {
				has_law = law_type:law_mechanim_freedom
			}
			add_journal_entry = { type = je_mechanim_2 }
		}
		trigger_event = { id = mechanim_revolt.1 popup = yes }
		custom_tooltip = mechanim_je_1_fail_tt
	}

	timeout = 1095

	on_timeout = {
		trigger_event = { id = mechanim_revolt.2 popup = yes }
		custom_tooltip = mechanim_je_1_timeout_tt
		set_variable = {
			name = mechanim_je_completed
			value = flag
		}
		set_variable = {
			name = mechanim_wrongs_enforced
			value = flag
		}
		if = {
			limit = {
				has_law = law_type:law_mechanim_freedom
			}
			add_journal_entry = { type = je_mechanim_2 }
		}

	}

	on_complete = {
		trigger_event = { id = mechanim_revolt.3 popup = yes }
		custom_tooltip = mechanim_je_1_complete_tt
		set_variable = {
			name = mechanim_je_completed
			value = flag
		}
	}

	should_be_pinned_by_default = yes
}

je_mechanim_2 = {
	icon = "gfx/interface/icons/event_icons/event_portrait.dds"
	
	group = je_group_internal_affairs

	immediate = {
		set_variable = {
			name = abolishing_mechanim_slavery_var
			value = 0
		}
	}

	on_monthly_pulse = {
		effect = {
			if = {
				limit = {
					has_law = law_type:law_mechanim_freedom
					NOT = {
						any_civil_war = {
							civil_war_progress > 0.5
							is_civil_war_type = revolution
						}
					}
					has_variable = mechanim_je_completed
				}
				change_variable = {
					name = abolishing_mechanim_slavery_var
					add = 1
				}
			}
		}
	}

	complete = {
		scope:journal_entry = {
			is_goal_complete = yes
		}
		has_law = law_type:law_mechanim_freedom
		NOT = {
			any_civil_war = {
				civil_war_progress > 0.5
				is_civil_war_type = revolution
			}
		}
		has_variable = mechanim_je_completed
	}

	on_complete = {
		set_variable = stamped_out_monarchy_var
		trigger_event = {
			id = mechanim_revolt.7
		}
	}

	invalid = {
		NOt = {
			is_enacting_law = law_type:law_mechanim_freedom
		}
	}

	current_value = {
		value = root.var:abolishing_mechanim_slavery_var
	}

	goal_add_value = {
		add = 60
	}

	progressbar = yes

	weight = 100
}
