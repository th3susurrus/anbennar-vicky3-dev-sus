﻿
ig_trait_kalyin_praxes = {
	icon = "gfx/interface/icons/ig_trait_icons/be_fruitful_and_multiply.dds"
	min_approval = loyal
	
	modifier = {
		state_radicals_from_political_movements_mult = -0.15
	}
}

ig_trait_kalyin_one_truth = {
	icon = "gfx/interface/icons/ig_trait_icons/divine_right.dds"
	min_approval = happy
	
	modifier = {
		state_conversion_mult = 0.15
	}
}

ig_trait_kalyin_inequality = {
	icon = "gfx/interface/icons/ig_trait_icons/pious_fiction.dds"
	max_approval = unhappy
	
	modifier = {
		country_radicalism_increases_violent_hostility_mult = 0.1
		country_radicalism_increases_cultural_erasure_mult = 0.1
	}
}


#Runefather Worship
ig_trait_to_find_salvation = {
	icon = "gfx/interface/icons/ig_trait_icons/be_fruitful_and_multiply.dds"
	min_approval = loyal
	
	modifier = {
		country_loyalism_increases_full_acceptance_mult = 0.15
	}
}
ig_trait_heed_his_word = {
	icon = "gfx/interface/icons/ig_trait_icons/divine_right.dds"
	min_approval = happy
	
	modifier = {
		state_conversion_mult = 0.2
	}
}
ig_trait_purge_the_heretic = {
	icon = "gfx/interface/icons/ig_trait_icons/pious_fiction.dds"
	max_approval = unhappy
	
	modifier = {
		country_radicalism_increases_full_acceptance_mult = 0.15
	}
}