﻿
ig_trait_self_supplied_arms = {
	icon = "gfx/interface/icons/ig_trait_icons/veteran_consultation.dds"
	min_approval = happy
	
	modifier = {
		country_military_goods_cost_mult = -0.15
	}
}





#Konolkhatep
ig_trait_ahati_glory = {
	icon = "gfx/interface/icons/ig_trait_icons/patriotic_fervor.dds"
	min_approval = loyal
	
	modifier = {
		unit_offense_mult = 0.1
		country_military_goods_cost_mult = -0.1
	}
}
ig_trait_ahati_support = {
	icon = "gfx/interface/icons/ig_trait_icons/veteran_consultation.dds"
	min_approval = happy
	
	modifier = {
		country_authority_mult = 0.05
		country_military_tech_research_speed_mult = 0.05
	}
}

ig_trait_rebel_ahati = {
	icon = "gfx/interface/icons/ig_trait_icons/materiel_waste.dds"
	max_approval = unhappy
	
	modifier = {
		building_training_rate_mult = -0.1
		state_radicals_from_political_movements_mult = 0.15
	}
}