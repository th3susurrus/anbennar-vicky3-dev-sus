﻿building_st_torrieths_bastion_of_the_god_fragment = {
	building_group = bg_monuments
	icon = "gfx/interface/icons/building_icons/vatican_city.dds"
	expandable = no
	buildable = no
	downsizeable = no
	unique = yes
	locator = "vatican_city_locator"
	
	entity_not_constructed = { }
	entity_under_construction = { "building_construction_3x3_entity" }
	entity_constructed = { "wonder_vatican_city_01_entity"}
	
	city_gfx_interactions = {
		clear_size_area = yes
		size = 6
	
	}
	
	production_method_groups = {
		pmg_base_building_st_torrieths_bastion_of_the_god_fragment
	}
	
	possible = {
		state_region = s:STATE_PASHAINE
	}
	
	required_construction = construction_cost_monument

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
}

building_great_tower_of_vis = {
	building_group = bg_monuments
	icon = "gfx/interface/icons/building_icons/big_ben.dds"
	expandable = no
	downsizeable = no
	buildable = no
	unique = yes
	locator = "big_ben_locator"
	
	entity_not_constructed = { }
	entity_under_construction = { "building_construction_2x2_entity" }
	entity_constructed = { "wonder_big_ben_01_entity"}
	
	city_gfx_interactions = {
		clear_size_area = yes
		size = 2
	}
	
	production_method_groups = {
		pmg_base_building_great_tower_of_vis
	}
	
	required_construction = construction_cost_monument
	
	possible = {
		state_region = s:STATE_THILVIS
	}
	
	ai_value = 10000

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
}

building_power_bloc_statue = {

	building_group = bg_monuments

	city_type = city

	levels_per_mesh = 1

	expandable = no
	
	statue = yes
	centerpiece_mesh_weight = 1

	production_method_groups = {
		pmg_base_building_power_bloc_statue
	}

	icon = "gfx/interface/icons/building_icons/building_powerblock_statue.dds"

	required_construction = construction_cost_monument

	can_build_government = {
		always = yes
	}

	can_build_private = {
		always = no
	}	
	
	possible = {
		owner = {
			is_in_power_bloc = yes
		}
		has_dlc_feature = power_bloc_features
	}

	ai_value = {
		value = 0
		
		if = {
			limit = { 
				is_incorporated = yes	
				state_population >= 500000
			}
			
			add = {
				value = state_population
				divide = 1000
				max = 5000
			}
		}

		if = {
			limit = { is_capital = yes }
			add = 2500
		}		
					
		owner = {
			if = {
				limit = { is_power_bloc_leader = yes }
				multiply = 2.0
			}						
			
			if = {
				limit = { country_rank < rank_value:major_power }
				multiply = 0.0
			}
			else_if = {
				limit = { country_rank < rank_value:great_power }
				multiply = 0.5
			}			
		}
	}

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
}