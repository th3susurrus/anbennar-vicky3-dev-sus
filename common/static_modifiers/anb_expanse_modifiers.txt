﻿rr_status_quo = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    country_prestige_mult = 0.1
    country_influence_mult = 0.1
}

rr_victory = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
    country_prestige_add = 20
}

rr_republic_lean = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    interest_group_pol_str_mult = 0.2
}

rr_junta_lean = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
    interest_group_pol_str_mult = 0.2
}

rr_elethael_victory = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
    country_prestige_add = 20
}
