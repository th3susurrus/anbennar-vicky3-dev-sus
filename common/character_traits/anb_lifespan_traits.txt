﻿


#These are all given through the default character template. Which means if you're creating a new character template to use, you MUST manually add the relevant lifespan traits.


#Not naturally given, manual only
trait_immortal = {
	type = condition
	texture = "gfx/interface/icons/character_trait_icons/imposing.dds"

	possible = {
		always = no
	}

	weight = 0
	value = 0
}



trait_elf = {
	type = condition
	texture = "gfx/interface/icons/character_trait_icons/imposing.dds"

	character_modifier = {
		character_health_add = 3
	}

	possible = {
		always = no
	}

	weight = 0
	value = 0
}