﻿namespace = goblin_tide


#third invasion reinforcements
goblin_tide.1 = {
	type = country_event
	placement = root
	
	title = goblin_tide.1.t
	desc =  goblin_tide.1.d
	flavor = goblin_tide.1.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 3

	option = { 
		name = goblin_tide.1.a
		default_option = yes

		custom_tooltip = goblin_tide.1.tooltip
	}
		
}
