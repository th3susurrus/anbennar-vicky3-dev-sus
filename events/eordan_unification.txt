﻿namespace = eordan_unification

# Unified Eordand
eordan_unification.1 = {
	type = country_event
	placement = ROOT

	title = eordan_unification.1.t
	desc = eordan_unification.1.d
	flavor = eordan_unification.1.f

	event_image = {
		video = "europenorthamerica_springtime_of_nations"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/waving_flag.dds"

	duration = 3

	trigger = {
	}

	immediate = {
		ruler.interest_group = {
			save_scope_as = ruler_ig_scope
		}
		random_country = {
			limit = {
				any_scope_character = {
					has_variable = mheromar_var
				}
				c:B87 !=this
			}
			trigger_event = mheromar_events.7
		}
		every_subject_or_below = {
			limit = {
				any_primary_culture = {
					has_discrimination_trait = eordan
					cu:fograc != this
				}
			}
			root = { annex_with_incorporation = prev }
		}
	}
	
	after = {
		hidden_effect = {
			if = {
				limit = { NOT = {	has_global_variable = mheromar_death } }
				trigger_event = {
					id = mheromar_events.5
					months = 1
				}
			}
		}
	}

	option = {
		name = eordan_unification.1.a
		default_option = yes
		give_claims_on_eordand = yes
		
		# prestige
		add_modifier = {
			name = eordan_unifier_prestige
			months = long_modifier_time
		}
	}
	option = {
		name = eordan_unification.1.b
		give_claims_on_eordand = yes
		
		# ruler IG attraction
		scope:ruler_ig_scope = {
			add_modifier = {
				name = eordan_unifiers_ig
				months = long_modifier_time
			}
		}
	}
}

# JE Start Up
eordan_unification.2 = {
	type = country_event
	placement = ROOT

	title = eordan_unification.2.t
	desc = eordan_unification.2.d
	flavor = eordan_unification.2.f

	event_image = {
		video = "unspecific_signed_contract"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/waving_flag.dds"

	duration = 3

	trigger = {
	}

	option = {
		name = eordan_unification.2.a
		default_option = yes
	}

}

# Diplo Unification
eordan_unification.3 = {
	type = country_event
	placement = ROOT

	title = eordan_unification.3.t
	desc = eordan_unification.3.d
	flavor = eordan_unification.3.f

	event_image = {
		video = "unspecific_politicians_arguing"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/waving_flag.dds"

	duration = 3

	trigger = {
	}

	option = {
		name = eordan_unification.3.a
		default_option = yes
		
		set_state_religion = rel:united_season_court
		
		custom_tooltip = {
			text = unify_the_season_tt
			every_state = {
				every_scope_pop = {
					limit = {
						OR = {
							has_pop_religion = spring_court
							has_pop_religion = summer_court
							has_pop_religion = autumn_court
							has_pop_religion = winter_court
							has_pop_religion = eordellon
						}
					}
					change_pop_religion = {
						target = rel:united_season_court
						value = 1.0
					}
				}
			}
		}
		
	}

}