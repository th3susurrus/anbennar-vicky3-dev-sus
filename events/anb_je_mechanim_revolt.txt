﻿namespace = mechanim_revolt

mechanim_revolt.1 = {
	type = country_event
	placement = root
	
	title = mechanim_revolt.1.t
	desc =  mechanim_revolt.1.d
	flavor = mechanim_revolt.1.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		#triggered by JE
	}

	immediate = {
		random_scope_state = {
			save_scope_as = rebellion_state
		}
		create_country = {
			tag = owner # this is to make the tag stay the same
			origin = owner # this is to make the tag stay the same
			state = scope:rebellion_state #this is for capital
			on_created = { # on_created removes any previous set scope no matter where so a new scope must be set
				save_scope_as = mechanim_revolt_nation
				random_country = { 
					limit = {
						has_variable = mechanim_oppressor_revolt #change variable to one from the je
					}
					save_scope_as = mechanim_oppreser_scope
				}
				random_scope_state = { #does work need to figure out a way to check states of owner
						limit = {
							owner = scope:mechanim_oppreser_scope
						} 
						set_state_owner = scope:mechanim_revolt_nation
					}
				}
				remove_variable = mechanim_oppressor_revolt #change variable
				activate_law = law_type:law_mechanim_freedom
				create_diplomatic_play = { #this causes the civil war
					type = dp_revolution
					target_state = scope:mechanim_oppreser_scope.capital
					annex_as_civil_war = yes
					escalation = 80
				}			
			}
		owner = {
			remove_variable = mechanim_oppressor_revolt  #change variable
		}
	}

	option = { 
		name = obsidian_invasions.2.a
		default_option = yes	
	}			
}

mechanim_revolt.2 = {
	type = country_event
	placement = root
	
	title = mechanim_revolt.2.t
	desc =  mechanim_revolt.2.d
	flavor = mechanim_revolt.2.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		#triggered by JE
	}
	immediate = {
		if = {
			limit = {
				has_law = law_type:law_sapience_mechanim_compromise
			}
			activate_law = law_type:law_sapience_mechanim_compromise_enforced 
		}
		else_if = {
			limit = {
				has_law = law_type:law_sapience_mechanim_unrecognized
			}
			activate_law = law_type:law_sapience_mechanim_unrecognized_enforced 
		}
	}
	option = { 
		name = mechanim_revolt.2.a
		default_option = yes
		add_modifier = {
			name = mechanim_timeout
			years = 5
		}	
	}
	option = {
		name = mechanim_revolt.2.b
		add_modifier = {
			name = mechanim_timeout
			years = 5
		}	
		activate_law = law_type:law_mechanim_banned
	}			
}

mechanim_revolt.3 = {
	type = country_event
	placement = root
	
	title = mechanim_revolt.3.t
	desc =  mechanim_revolt.3.d
	flavor = mechanim_revolt.3.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		#triggered by JE
	}

	option = { 
		name = mechanim_revolt.3.a
		default_option = yes
		ig:ig_industrialists = {
			remove_ideology = ideology_machine_servitude
		}
		ig:ig_landowners = {
			remove_ideology = ideology_machine_servitude
		}
		ig:ig_petty_bourgeoisie  = {	
			remove_ideology = ideology_pb_machine_servitude
		}
		ig:ig_rural_folk = {
			remove_ideology = ideology_luddite_mechanim
		}
		ig:ig_trade_unions = {
			remove_ideology = ideology_anti_mechanim_trade
		}
		add_modifier = {
			name = je_mechanim_complete
			years = 10
		}	
	}			
}

mechanim_revolt.4 = {
	type = country_event
	placement = root
	
	title = mechanim_revolt.4.t
	desc =  mechanim_revolt.4.d
	flavor = mechanim_revolt.4.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		#triggered by JE
	}

	option = {
		name = mechanim_revolt.4.a
		default_option = yes
		capital = {
			add_radicals_in_state = {
				value = medium_radicals
				pop_type = laborers
			}
		}
	}
	option = {
		name =  mechanim_revolt.4.b
		trigger = {
			any_interest_group = {
				is_in_government = yes
				OR = {
					law_stance = {
						law = law_type:law_mechanim_freedom 
						value = approve
					}
					law_stance = {
						law = law_type:law_mechanim_freedom 
						value = strongly_approve
					}
					leader = {
						has_ideology = ideology:ideology_mechanim_liberator
					}
				}
			}
		}
		capital = {
			add_radicals_in_state = {
				value = small_radicals
				pop_type = laborers
			}
		}
		custom_tooltip = {
			text = mechanim_success_var_tick_tt
			set_variable = { name = mechanim_success_progress_var_add value = 0.25 }
		}
	}			
}

mechanim_revolt.5 = {
	type = country_event
	placement = root
	
	title = mechanim_revolt.5.t
	desc =  mechanim_revolt.5.d
	flavor = mechanim_revolt.5.f
	hidden = yes
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_automatories
			}
		}
	}

	immediate = {
		every_scope_state = {
			limit = {
				any_scope_building = {
					is_building_type = building_automatories
				}
			}
			create_pop = {
                culture = mechanim
                pop_type = labourer
                literacy_rate = 0.35
                working_adult_ratio = 1.0
                size = 70000
            }
		}
	}			
}

mechanim_revolt.6 = { #mechanim escape event 
	type = country_event
	placement = root
	
	title = mechanim_revolt.6.t
	desc =  mechanim_revolt.6.d
	flavor = mechanim_revolt.6.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 1
	trigger = {
		any_scope_state = {
			sg:automata = {
				state_goods_consumption > 1
			}
		}
	}

	immediate = {
		random_scope_state = {
			limit = {
				sg:automata = {
					state_goods_consumption > 1
				}
			}
			create_pop = {
                culture = mechanim
                pop_type = labourer
                literacy_rate = 0.35
                size = 1000
            }			
            save_scope_as = mechanim_escape_state
		}

	}

	option = {  #do nothing radical time
		name = mechanim_revolt.6.a
		default_option = yes
		if = { #blame and use workers to stop mechanim leaving
			limit = {
				OR = {
					has_law = law_type:law_sapience_mechanim_unrecognized_enforced
					has_law = law_type:law_sapience_mechanim_unrecognized
				}
			}
			scope:mechanim_escape_state = {
				add_radicals_in_state = {
					strata = rich
					value = large_radicals
				}
			}
		}
		else = {
			scope:mechanim_escape_state = {
				add_radicals_in_state = {
					strata = rich
					value = medium_radicals
				}
			}
		}
	}
	option = {  #blame the rich and use their funds to stop mechanim
		name = mechanim_revolt.6.b
		trigger = {
			OR = {
				has_law = law_type:law_dedicated_police
				has_law = law_type:law_militarized_police
			}
		}		
		if = {
			limit = {
				OR = {
					has_law = law_type:law_sapience_mechanim_unrecognized_enforced
					has_law = law_type:law_sapience_mechanim_unrecognized
				}
			}
			scope:mechanim_escape_state = {
				add_radicals_in_state = {
					strata = rich
					value = medium_radicals
				}
			}
		}
		else = {
			scope:mechanim_escape_state = {
				add_radicals_in_state = {
					strata = rich
					value = small_radicals
				}
			}
		}
	}						
}

mechanim_revolt.7 = { #de politicize shit
	type = country_event
	placement = root
	title = mechanim_revolt.7.t
	desc = mechanim_revolt.7.d
	flavor = mechanim_revolt.7.f

	duration = 3

	event_image = {
		video = "europenorthamerica_london_center"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/waving_flag.dds"

	trigger = {
		# triggered by je_abolish_monarchy
	}

	option = {
		name = mechanim_revolt.7.a
		default_option = yes
		ig:ig_industrialists = {
			remove_ideology = ideology_machine_servitude
		}
		ig:ig_landowners = {
			remove_ideology = ideology_machine_servitude
		}
		ig:ig_petty_bourgeoisie  = {	
			remove_ideology = ideology_pb_machine_servitude
		}
		ig:ig_rural_folk = {
			remove_ideology = ideology_luddite_mechanim
		}
		ig:ig_trade_unions = {
			remove_ideology = ideology_anti_mechanim_trade
		}
		every_interest_group = {
			limit = {
				leader = {
					OR = {
						has_ideology =	ideology_mechanim_slaver
						has_ideology =  ideology_mechanim_liberator 
					}
				}
			}
			leader = {
				set_ideology = ideology:ideology_moderate
			}
		}
	}
}